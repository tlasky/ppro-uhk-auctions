package cz.uhk.tlaskal.auction.auction.controllers;

import cz.uhk.tlaskal.auction.auction.forms.AuctionForm;
import cz.uhk.tlaskal.auction.auction.models.Auction;
import cz.uhk.tlaskal.auction.auction.models.AuctionBid;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionBidRepository;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionRepository;
import cz.uhk.tlaskal.auction.auction.repositories.UserRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Controller
public class AuctionController {
  @Autowired
  AuctionRepository auctionRepository;

  @Autowired
  AuctionBidRepository auctionBidRepository;

  @Autowired
  UserRepository userRepository;


  public void validateAuction(Auction auction) {
    if (auction == null || !auction.isValid()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  public Double auctionPrice(Auction auction) {
    AuctionBid lastBid = auctionBidRepository.findFirstByAuctionOrderByValueDesc(auction);
    return lastBid != null ? lastBid.getValue() : auction.getStartingPrice();
  }

  @GetMapping("/auctions")
  public String auctionList(Model model) {
    List<Auction> auctions = auctionRepository.findAllValid(new DateTime().toDate());

    HashMap<Auction, Double> auctionsPrices = new HashMap<>();
    for (Auction auction : auctions) {
      auctionsPrices.put(auction, auctionPrice(auction));
    }

    model.addAttribute("auctions", auctions);
    model.addAttribute("auctionsPrices", auctionsPrices);
    return "auction_list";
  }


  @GetMapping("/auction/{id}")
  public String auctionDetail(@PathVariable(required = true) String id, Principal principal, Model model) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);

    model.addAttribute("auction", auction);
    model.addAttribute("auctionPrice", auctionPrice(auction));
    model.addAttribute("currentUser", userRepository.findByUsername(principal.getName()));
    return "auction_detail";
  }

  @PostMapping("/auction/{id}/bid")
  public String auctionBid(@PathVariable(required = true) String id, @RequestParam(required = true, name = "bidValueIncrement") String bidValueIncrement, Principal principal, Model model, RedirectAttributes redirectAttributes) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);
    redirectAttributes.addAttribute("id", id);

    if (!auction.getOwner().getUsername().equals(principal.getName()) && auction.hasStarted()) {
      AuctionBid bid = new AuctionBid(
          auctionPrice(auction) + Double.parseDouble(bidValueIncrement),
          new DateTime().toDate(),
          auction,
          userRepository.findByUsername(principal.getName())
      );
      auctionBidRepository.save(bid);
    }

    return "redirect:/auction/{id}";
  }

  @GetMapping("/auction/create")
  public String auctionCreate(AuctionForm auctionForm) {
    return "auction_create";
  }

  @PostMapping("/auction/create")
  public String auctionCreateAction(@Valid AuctionForm auctionForm, Errors errors, Model model, Principal principal, RedirectAttributes redirectAttributes) {
    if (errors.hasErrors()) {
      return "auction_create";
    }
    Auction auction = auctionRepository.save(new Auction(
        auctionForm.getName(),
        auctionForm.getDescription(),
        auctionForm.getStartsAt(),
        auctionForm.getEndsAt(),
        auctionForm.getStartingPrice(),
        userRepository.findByUsername(principal.getName()),
        Collections.emptyList()
    ));
    redirectAttributes.addAttribute("id", auction.getId());
    return "redirect:/auction/{id}";
  }

  @GetMapping("/auction/{id}/edit")
  public String auctionEdit(@PathVariable(required = true) String id, AuctionForm auctionForm, Model model, Principal principal, RedirectAttributes redirectAttributes) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);
    redirectAttributes.addAttribute("id", auction.getId());

    if (!auction.getOwner().getUsername().equals(principal.getName())) {
      return "redirect:/auction/{id}";
    }

    auctionForm.setName(auction.getName());
    auctionForm.setDescription(auction.getDescription());
    auctionForm.setStartsAt(auction.getStartsAt());
    auctionForm.setEndsAt(auction.getEndsAt());
    auctionForm.setStartingPrice(auction.getStartingPrice());

    return "auction_edit";
  }

  @PostMapping("/auction/{id}/edit")
  public String auctionEditAction(@PathVariable(required = true) String id, AuctionForm auctionForm, Principal principal, RedirectAttributes redirectAttributes) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);
    redirectAttributes.addAttribute("id", auction.getId());

    if (!auction.getOwner().getUsername().equals(principal.getName())) {
      return "redirect:/auction/{id}";
    }

    auction.setName(auctionForm.getName());
    auction.setDescription(auctionForm.getDescription());
    auction.setStartsAt(auctionForm.getStartsAt());
    auction.setEndsAt(auctionForm.getEndsAt());
    auction.setStartingPrice(auctionForm.getStartingPrice());
    auctionRepository.save(auction);

    return "redirect:/auction/{id}";
  }

  @GetMapping("/auction/{id}/delete")
  public String auctionDelete(@PathVariable(required = true) String id, Model model, Principal principal, RedirectAttributes redirectAttributes) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);
    redirectAttributes.addAttribute("id", auction.getId());

    if (!auction.getOwner().getUsername().equals(principal.getName())) {
      return "redirect:/auction/{id}";
    }

    model.addAttribute("auction", auction);

    return "auction_delete";
  }

  @PostMapping("/auction/{id}/delete")
  public String auctionDeleteAction(@PathVariable(required = true) String id, Principal principal) {
    Auction auction = auctionRepository.findById(Integer.parseInt(id));
    validateAuction(auction);

    if (!auction.getOwner().getUsername().equals(principal.getName())) {
      return "redirect:/auction/{id}";
    }

    auctionRepository.delete(auction);

    return "redirect:/auctions";
  }
}
