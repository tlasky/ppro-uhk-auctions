package cz.uhk.tlaskal.auction.auction.repositories;

import cz.uhk.tlaskal.auction.auction.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.security.Principal;
import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {
  public User findByUsername(String username);

  public List<User> findAll();
}
