package cz.uhk.tlaskal.auction.auction.forms;


import java.util.Date;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;

@Data
public class AuctionForm {
  @NotNull
  @NotEmpty
  @Size(min = 5, max = 50)
  private String name;

  @NotNull
  @NotEmpty
  @Size(min = 5)
  private String description;

  @FutureOrPresent
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private Date startsAt;

  @Future
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private Date endsAt;

  @PositiveOrZero
  private Double startingPrice;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getStartsAt() {
    return startsAt;
  }

  public void setStartsAt(Date startsAt) {
    this.startsAt = startsAt;
  }

  public Date getEndsAt() {
    return endsAt;
  }

  public void setEndsAt(Date endsAt) {
    this.endsAt = endsAt;
  }

  public Double getStartingPrice() {
    return startingPrice;
  }

  public void setStartingPrice(Double startingPrice) {
    this.startingPrice = startingPrice;
  }
}
