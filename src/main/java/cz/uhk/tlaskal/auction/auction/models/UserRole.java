package cz.uhk.tlaskal.auction.auction.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class UserRole {
  @Id
  @GeneratedValue
  private int id;

  private String name;
  private String description;

  @ManyToMany
  private List<User> users;

  public UserRole(String name, String description, List<User> users) {
    this.name = name;
    this.description = description;
    this.users = users;
  }

  public UserRole() {}


  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public List<User> getUsers() {
    return users;
  }
}
