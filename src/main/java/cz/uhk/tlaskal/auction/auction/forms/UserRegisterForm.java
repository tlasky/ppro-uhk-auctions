package cz.uhk.tlaskal.auction.auction.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class UserRegisterForm {
  @NotNull
  @NotEmpty
  @Size(min = 5, max = 10)
  private String username;

  @NotNull
  @NotEmpty
  @Size(min = 8, max=20)
  @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
  private String password;

  @NotNull
  @NotEmpty
  @Size(min = 8, max=20)
  private String passwordAgain;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordAgain() {
    return passwordAgain;
  }

  public void setPasswordAgain(String passwordAgain) {
    this.passwordAgain = passwordAgain;
  }
}
