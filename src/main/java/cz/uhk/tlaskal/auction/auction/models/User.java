package cz.uhk.tlaskal.auction.auction.models;


import javax.persistence.*;
import java.util.List;

@Entity
public class User {
  @Id
  @GeneratedValue
  private int id;

  private String username;
  private String passwordHash;

  @ManyToMany
  private List<UserRole> roles;

  @OneToMany
  private List<Auction> auctions;

  @OneToMany
  private List<AuctionBid> auctionBids;

  public User(String username, String passwordHash, List<UserRole> roles) {
    this.username = username;
    this.passwordHash = passwordHash;
    this.roles = roles;
  }

  public User() {

  }

  public int getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getPasswordHash() {
    return passwordHash;
  }

  public List<UserRole> getRoles() {
    return roles;
  }


}
