package cz.uhk.tlaskal.auction.auction.repositories;

import cz.uhk.tlaskal.auction.auction.models.Auction;
import cz.uhk.tlaskal.auction.auction.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AuctionRepository extends JpaRepository<Auction, String> {
  public Auction findByName(String name);

  public Auction findById(int id);

  public List<Auction> findAllByOwnerOrderByStartsAt(User owner);

  @Query(value = "from Auction a where a.endsAt>:now")
  public List<Auction> findAllValid(@Param("now") Date now);
}
