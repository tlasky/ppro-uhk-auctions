package cz.uhk.tlaskal.auction.auction.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class AuctionBid {
  @Id
  @GeneratedValue
  private int id;

  private Double value;
  private Date bidAt;

  @ManyToOne
  private Auction auction;

  @ManyToOne
  private User bidBy;

  public AuctionBid(Double value, Date bidAt, Auction auction, User bidBy) {
    this.value = value;
    this.bidAt = bidAt;
    this.auction = auction;
    this.bidBy = bidBy;
  }

  public AuctionBid() {
  }

  public int getId() {
    return id;
  }

  public Double getValue() {
    return value;
  }

  public Date getBidAt() {
    return bidAt;
  }

  public Auction getAuction() {
    return auction;
  }

  public User getBidBy() {
    return bidBy;
  }
}
