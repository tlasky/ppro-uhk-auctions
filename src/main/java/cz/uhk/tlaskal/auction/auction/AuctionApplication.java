package cz.uhk.tlaskal.auction.auction;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "cz.uhk.tlaskal.auction.auction")
public class AuctionApplication {
  public static void main(String[] args) {
    SpringApplication.run(AuctionApplication.class, args);
  }
}
