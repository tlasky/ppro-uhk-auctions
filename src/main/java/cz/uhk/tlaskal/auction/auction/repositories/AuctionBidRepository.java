package cz.uhk.tlaskal.auction.auction.repositories;

import cz.uhk.tlaskal.auction.auction.models.Auction;
import cz.uhk.tlaskal.auction.auction.models.AuctionBid;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuctionBidRepository extends JpaRepository<AuctionBid, String> {

  public AuctionBid findByAuction(Auction auction);

  public AuctionBid findFirstByAuctionOrderByValueDesc(Auction auction);

  public List<AuctionBid> findAllByAuction(Auction auction);

  public List<AuctionBid> findByAuctionOrderByValue(Auction auction);
}
