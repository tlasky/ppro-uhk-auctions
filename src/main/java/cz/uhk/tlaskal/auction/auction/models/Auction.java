package cz.uhk.tlaskal.auction.auction.models;

import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Auction {
  @Id
  @GeneratedValue
  private int id;

  private String name;
  private String description;
  private Date startsAt;
  private Date endsAt;
  private Double startingPrice;

  @ManyToOne
  private User owner;

  @OneToMany(fetch = FetchType.LAZY)
  private List<AuctionBid> bids;

  public Boolean isValid() {
    DateTime endsAt = new DateTime(getEndsAt().getTime());
    return DateTime.now().isBefore(endsAt);
  }

  public Boolean hasStarted() {
    DateTime startsAt = new DateTime(getStartsAt().getTime());
    return DateTime.now().isAfter(startsAt);
  }

  public Auction(String name, String description, Date startsAt, Date endsAt, Double startingPrice, User owner, List<AuctionBid> bids) {
    this.name = name;
    this.description = description;
    this.startsAt = startsAt;
    this.endsAt = endsAt;
    this.startingPrice = startingPrice;
    this.owner = owner;
    this.bids = bids;
  }

  public Auction() {
  }


  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Date getStartsAt() {
    return startsAt;
  }

  public Date getEndsAt() {
    return endsAt;
  }

  public Double getStartingPrice() {
    return startingPrice;
  }

  public User getOwner() {
    return owner;
  }

  public List<AuctionBid> getBids() {
    return bids;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setStartsAt(Date startsAt) {
    this.startsAt = startsAt;
  }

  public void setEndsAt(Date endsAt) {
    this.endsAt = endsAt;
  }

  public void setStartingPrice(Double startingPrice) {
    this.startingPrice = startingPrice;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public void setBids(List<AuctionBid> bids) {
    this.bids = bids;
  }
}
