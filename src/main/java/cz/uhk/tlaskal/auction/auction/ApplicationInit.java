package cz.uhk.tlaskal.auction.auction;

import cz.uhk.tlaskal.auction.auction.models.Auction;
import cz.uhk.tlaskal.auction.auction.models.AuctionBid;
import cz.uhk.tlaskal.auction.auction.models.User;
import cz.uhk.tlaskal.auction.auction.models.UserRole;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionBidRepository;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionRepository;
import cz.uhk.tlaskal.auction.auction.repositories.UserRepository;
import cz.uhk.tlaskal.auction.auction.repositories.UserRoleRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ApplicationInit {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserRoleRepository userRoleRepository;

  @Autowired
  private AuctionRepository auctionRepository;

  @Autowired
  private AuctionBidRepository auctionBidRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @PostConstruct
  public void init() {
    System.out.println("Initialising...");
    initApp();
  }

  private void initApp() {
    UserRole adminRole = userRoleRepository.findByName("admin");
    if (adminRole == null) {
      adminRole = new UserRole("admin", "Administrator", new ArrayList<>());
      userRoleRepository.save(adminRole);
    }

    User admin = userRepository.findByUsername("tlasky");
    if (admin == null) {
      admin = new User(
          "tlasky",
          passwordEncoder.encode("123"),
          List.of(adminRole)
      );
      userRepository.save(admin);
    }

    User user = userRepository.findByUsername("test_user");
    if (user == null) {
      user = new User(
          "test_user",
          passwordEncoder.encode("123"),
          Collections.emptyList()
      );
      userRepository.save(user);
    }

    Auction auction = auctionRepository.findByName("Test auction");
    if (auction == null) {
      auction = new Auction(
          "Test auction",
          "This is a testing auction",
          new DateTime().toDate(),
          new DateTime().plusDays(7).toDate(),
          10.,
          admin,
          Collections.emptyList()
      );
      auctionRepository.save(auction);
    }

    List<AuctionBid> auctionBids = auctionBidRepository.findAllByAuction(auction);
    AuctionBid auctionBid = auctionBids.size() > 0 ? auctionBids.get(0) : null;
    if (auctionBid == null) {
      auctionBid = new AuctionBid(
          15.,
          new DateTime().toDate(),
          auction,
          user
      );
      auctionBidRepository.save(auctionBid);
    }
  }
}
