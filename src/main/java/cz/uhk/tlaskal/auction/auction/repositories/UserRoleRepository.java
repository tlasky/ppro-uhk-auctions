package cz.uhk.tlaskal.auction.auction.repositories;

import cz.uhk.tlaskal.auction.auction.models.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, String> {
  public UserRole findByName(String name);
}
