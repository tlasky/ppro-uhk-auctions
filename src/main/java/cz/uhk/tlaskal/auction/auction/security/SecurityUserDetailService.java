package cz.uhk.tlaskal.auction.auction.security;


import cz.uhk.tlaskal.auction.auction.models.User;
import cz.uhk.tlaskal.auction.auction.models.UserRole;
import cz.uhk.tlaskal.auction.auction.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class SecurityUserDetailService implements UserDetailsService {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  PasswordEncoder encoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username);
    if(user == null){
      throw new UsernameNotFoundException(String.format("User \"%s\" was not found.", username));
    }
    return new org.springframework.security.core.userdetails.User(
        user.getUsername(),
        user.getPasswordHash(),
        getGrantedAuthorities(user)
    );
  }

  public static Collection<GrantedAuthority> getGrantedAuthorities(User user){
    Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();
    /*for (UserRole role : user.getRoles()) {
      grantedAuthority.add(new SimpleGrantedAuthority(role.getName()));
    }*/
    return grantedAuthority;
  }
}