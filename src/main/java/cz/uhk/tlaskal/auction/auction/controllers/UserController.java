package cz.uhk.tlaskal.auction.auction.controllers;

import cz.uhk.tlaskal.auction.auction.forms.UserRegisterForm;
import cz.uhk.tlaskal.auction.auction.models.Auction;
import cz.uhk.tlaskal.auction.auction.models.AuctionBid;
import cz.uhk.tlaskal.auction.auction.models.User;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionBidRepository;
import cz.uhk.tlaskal.auction.auction.repositories.AuctionRepository;
import cz.uhk.tlaskal.auction.auction.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Controller
public class UserController {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AuctionRepository auctionRepository;

  @Autowired
  private AuctionBidRepository auctionBidRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @GetMapping("/login")
  public String userLogin() {
    return "user_login";
  }

  @GetMapping("/register")
  public String userRegister(UserRegisterForm userRegisterForm) {
    return "user_register";
  }

  @PostMapping("/register")
  public String userRegisterAction(@Valid UserRegisterForm userRegisterForm, Errors errors, Model model) {
    List<String> messages = new ArrayList<>();

    if (userRepository.findByUsername(userRegisterForm.getUsername()) != null) {
      messages.add(String.format("User with username %s already exists.", userRegisterForm.getUsername()));
    }
    if (!userRegisterForm.getPassword().equals(userRegisterForm.getPasswordAgain())) {
      messages.add("Passwords does not match.");
    }

    if (!errors.hasErrors() && messages.size() == 0) {
      userRepository.save(new User(
          userRegisterForm.getUsername(),
          passwordEncoder.encode(userRegisterForm.getPassword()),
          Collections.emptyList()
      ));
      return "redirect:/login";
    }

    model.addAttribute("messages", messages);
    return "user_register";
  }

  public Double auctionPrice(Auction auction) {
    AuctionBid lastBid = auctionBidRepository.findFirstByAuctionOrderByValueDesc(auction);
    return lastBid != null ? lastBid.getValue() : auction.getStartingPrice();
  }

  @GetMapping("/profile")
  public String userProfile(Model model, Principal principal) {
    User user = userRepository.findByUsername(principal.getName());
    List<Auction> auctions = auctionRepository.findAllByOwnerOrderByStartsAt(user);

    HashMap<Auction, Double> auctionsPrices = new HashMap<>();
    for (Auction auction : auctions) {
      auctionsPrices.put(auction, auctionPrice(auction));
    }

    model.addAttribute("user", user);
    model.addAttribute("ownedAuctions", auctions);
    model.addAttribute("auctionsPrices", auctionsPrices);
    return "user_profile";
  }

  @GetMapping("/users")
  public String userList(Model model) {
    model.addAttribute("users", userRepository.findAll());
    return "user_list";
  }
}
